using System.Collections.Generic;
using ExitGames.Client.Photon;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.Events;

namespace lab.photon
{
    public class PhotonEventsSenderReceiver : MonoBehaviour
    {
        private readonly byte gameStateEvent = 0;
        private readonly byte scoreStateEvent = 1;
        
        private readonly Dictionary<byte, UnityAction<object[]>> receiveEventsActions = new Dictionary<byte, UnityAction<object[]>>(); 

        private void Awake()
        {
            GameEvents.Instance.OnGameStateChanged += OnGameStateChanged;
            GameEvents.Instance.OnScoreUpdated += OnScoreUpdated;
            
            PhotonNetwork.NetworkingClient.EventReceived += OnEvent;
            
            receiveEventsActions.Add(gameStateEvent, OnParseGameState);
            receiveEventsActions.Add(scoreStateEvent, OnParseScore);
        }
        
        private void OnDestroy()
        {
            PhotonNetwork.NetworkingClient.EventReceived -= OnEvent;
        }

        #region Send
        
        private void OnGameStateChanged(GameState state, bool owner)
        {
            if (!owner)
                return;

            Debug.Log("Send state = " + state);
            
            Send(gameStateEvent, 
                new RaiseEventOptions { Receivers = ReceiverGroup.Others}, 
                new object[] {state});
        }
        
        private void OnScoreUpdated(ScoreData scoreData, bool owner)
        {
            if (!owner)
                return;
            
            Debug.Log("Send score = " + scoreData.AdminScore + " " + scoreData.PlayerScore);
            
            Send(scoreStateEvent, 
                 new RaiseEventOptions { Receivers = ReceiverGroup.Others}, 
                new object[] {scoreData.AdminScore, scoreData.PlayerScore});
        }

        private void Send(byte eventCode, RaiseEventOptions raiseEventOptions, object[] data)
        {
            SendOptions sendOptions = new SendOptions {Reliability = true};
            PhotonNetwork.RaiseEvent(eventCode, data, raiseEventOptions, sendOptions);
        }
        
        #endregion
        
        #region Receive
        
        private void OnEvent(EventData photonEvent)
        {
            byte eventCode = photonEvent.Code;
            
            if (receiveEventsActions.ContainsKey(eventCode))
            {
                Debug.Log("OnEvent receive " + eventCode);
                object[] data = (object[])photonEvent.CustomData;
                receiveEventsActions[eventCode].Invoke(data);
            }
        }
        
        #endregion

        #region Events parsers

        private void OnParseGameState(object[] data)
        {
            var state = (GameState) data[0];
            GameEvents.Instance.OnGameStateChanged?.Invoke(state, false);
            Debug.Log("Received game state  = " + state);
        }
        
        private void OnParseScore(object[] data)
        {
            var adminScore = (int) data[0];
            var playerScore = (int) data[1];
            
            var scoreData = new ScoreData {AdminScore = adminScore, PlayerScore = playerScore};
            
            GameEvents.Instance.OnScoreUpdated?.Invoke(scoreData, false);
            Debug.Log("Received score  = " + scoreData.AdminScore + ":" + scoreData.PlayerScore);
        }
        
        #endregion
        
        //RaiseEventOptions raiseEventOptions = new RaiseEventOptions { TargetActors = new int[] { other.ActorNumber }}; 
        
        
    }
}