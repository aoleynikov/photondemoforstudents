﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Photon.Pun;
using Photon.Realtime;
using UnityEngine;

namespace lab.photon
{
    public class LauncherNetwork : MonoBehaviourPunCallbacks
    {
        private GameSettings gameSettings;
        private bool connectedToMasterServer = false;
        private bool connectedToLobby = false;
        
        private List<RoomInfo> createdRooms = new List<RoomInfo>();
        
        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            
            NetworkEvents.Instance.OnConnectToNetwork += OnConnectToNetwork;
            NetworkEvents.Instance.OnCreateRoom += OnCreateRoom;
            NetworkEvents.Instance.OnJoinRoom += OnJoinRoom;
        }

        private void OnDestroy()
        {
            NetworkEvents.Instance.Dispose();
        }

        #region Requests
        
        /// <summary>
        /// Подключиться к master server и войти в лобби
        /// </summary>
        private void OnConnectToNetwork()
        {
            StartCoroutine(ConnectToNetworkRoutine());
        }

        /// <summary>
        /// Создать комнату
        /// </summary>
        private void OnCreateRoom(string roomName)
        {
            StartCoroutine(CreateRoomRoutine(roomName));
        }
        
        /// <summary>
        /// Подключиться к комнате
        /// </summary>
        private void OnJoinRoom(string roomName)
        {
            SetPlayerNetworkRole(gameSettings.NetworkSettings.NetworkRole);
            
            PhotonNetwork.JoinRoom(roomName);
        }

        /// <summary>
        /// Подключиться к master server
        /// </summary>
        private void ConnectToMasterServer()
        {
            PhotonNetwork.NickName = gameSettings.NetworkSettings.Login;
            PhotonNetwork.ConnectUsingSettings();
            Debug.Log("ConnectToMasterServer");
        }

        /// <summary>
        /// Подключиться к лобби
        /// </summary>
        private void JoinLobby()
        {
            PhotonNetwork.JoinLobby();
            Debug.Log("JoinLobby");
        }
        
        /// <summary>
        /// Создать комнату
        /// </summary>
        private void CreateRoom(string roomName)
        {
            if (RoomWithNameAlreadyExists(roomName))
            {
                Debug.LogError("Room with name " + roomName + " already exists");
                return;
            }
            
            RoomOptions options = new RoomOptions
            {
                MaxPlayers = gameSettings.NetworkSettings.MaxPlayersPerRoom,
                IsVisible = true,
                IsOpen = true,
                PlayerTtl = 0,
                EmptyRoomTtl = 0
            };

            SetPlayerNetworkRole(gameSettings.NetworkSettings.NetworkRole);
            
            PhotonNetwork.CreateRoom(roomName, options, null);
            
            Debug.Log("Create Room" + roomName);
        }

        /// <summary>
        /// Передать в фотон данные о сетевой ролди пользователя
        /// </summary>
        private void SetPlayerNetworkRole(NetworkRole networkRole)
        {
            ExitGames.Client.Photon.Hashtable customProperties = new ExitGames.Client.Photon.Hashtable();
            customProperties.Add("NetworkRole", gameSettings.NetworkSettings.NetworkRole.ToString());
            PhotonNetwork.SetPlayerCustomProperties(customProperties);
        }
        
        private bool RoomWithNameAlreadyExists(string roomName)
        {
            RoomInfo room  = createdRooms.FirstOrDefault(r => r.Name == roomName);
            return (room != null);
        }
        
        #endregion

        #region Coroutines

        private IEnumerator ConnectToNetworkRoutine()
        {
            yield return new WaitForEndOfFrame();
            ConnectToMasterServer();
            yield return new WaitUntil(() => connectedToMasterServer);
            JoinLobby();
        }
        
        private IEnumerator CreateRoomRoutine(string roomName)
        {
            yield return new WaitUntil(() => connectedToMasterServer);
            yield return new WaitUntil(() => connectedToLobby);

            CreateRoom(roomName);
        }

        #endregion
        
        #region Callbacks

        public override void OnConnectedToMaster()
        {
            connectedToMasterServer = true;
            Debug.Log("OnConnectedToMaster");
        }

        public override void OnJoinedLobby()
        {
            connectedToLobby = true;
            Debug.Log("OnJoinedLobby");
        }

        public override void OnJoinedRoom()
        {
            PhotonNetwork.LoadLevel(gameSettings.NetworkSettings.SceneToLoad);
        }
        
        public override void OnRoomListUpdate(List<RoomInfo> roomList)
        {
            createdRooms.Clear();
            createdRooms = roomList;
            
            NetworkEvents.Instance.OnRoomsListUpdated?.Invoke(roomList);
            Debug.Log("OnRoomListUpdate " + roomList.Count);
        }

        #endregion
        
    }
}
