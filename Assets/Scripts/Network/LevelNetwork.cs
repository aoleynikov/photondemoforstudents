using Photon.Pun;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace lab.photon
{
    public class LevelNetwork : MonoBehaviourPunCallbacks
    {
        [SerializeField] private int playersCount = 1;
        [SerializeField] private int spectatorsCount = 0;

        private GameSettings gameSettings;

        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
        }

        private void OnDestroy()
        {
            NetworkEvents.Instance.Dispose();
        }
        
        public override void OnPlayerEnteredRoom(Player otherPlayer)
        {
            if (gameSettings.NetworkSettings.NetworkRole != NetworkRole.Admin)
                return;
            
            var networkRole = otherPlayer.CustomProperties["NetworkRole"].ToString();

            if (networkRole == NetworkRole.Player.ToString())
            {
                playersCount++;
                
                // если игроков более двух, включая админа — вырубаем пользователя из сессии
                if (playersCount > 2)
                {
                    PhotonNetwork.CloseConnection(otherPlayer);
                }
                // если подключилось два игрока — начинаем игру
                else if (playersCount == 2)
                {
                    GameEvents.Instance.OnGameStateChanged?.Invoke(GameState.Game, true);
                }
            }
            else if (networkRole == NetworkRole.Spectator.ToString())
            {
                spectatorsCount++;
            }
        }

        public override void OnPlayerLeftRoom(Player otherPlayer)
        {
            if (gameSettings.NetworkSettings.NetworkRole != NetworkRole.Admin)
                return;
            
            var networkRole = otherPlayer.CustomProperties["NetworkRole"].ToString();
            
            if (networkRole == NetworkRole.Player.ToString())
            {
                playersCount--;

                if (playersCount == 1)
                {
                    GameEvents.Instance.OnGameStateChanged?.Invoke(GameState.Wait, true);
                }
            }
            else if (networkRole == NetworkRole.Spectator.ToString())
            {
                spectatorsCount--;
            }
        }
        
        public override void OnLeftRoom()
        {
            SceneManager.LoadScene(0);
        }
    }
}