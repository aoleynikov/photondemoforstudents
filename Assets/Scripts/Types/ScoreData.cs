namespace lab.photon
{
    public struct ScoreData
    {
        public int AdminScore;
        public int PlayerScore;        
    }
}