namespace lab.photon
{
    public enum GameState
    {
        Wait,
        Game,
        Finish
    }
}