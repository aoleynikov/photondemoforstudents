namespace lab.photon
{
    public enum NetworkRole
    {
        Admin,
        Player,
        Spectator
    }
}