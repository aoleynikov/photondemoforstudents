using UnityEngine;

namespace lab.photon
{
    [CreateAssetMenu(fileName = "LevelSettings", menuName = "Lab/LevelSettings")]
    public class LevelSettings : ScriptableObject
    {
        public int MaxScore = 5;
    }
}