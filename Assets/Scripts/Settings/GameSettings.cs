using UnityEngine;

namespace lab.photon
{
    public class GameSettings : MonoBehaviour
    {
        public NetworkSettings NetworkSettings;
        public LevelSettings LevelSettings;
    }
}