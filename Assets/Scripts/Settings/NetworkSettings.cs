using UnityEditor;
using UnityEngine;

namespace lab.photon
{
    [CreateAssetMenu(fileName = "NetworkSettings", menuName = "Lab/NetworkSettings")]
    public class NetworkSettings : ScriptableObject
    {
        public string Login;
        public NetworkRole NetworkRole;
        public byte MaxPlayersPerRoom = 4;
        public string SceneToLoad;
    }
}