using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine.Events;

namespace lab.photon
{
    public class NetworkEvents
    {
        private static NetworkEvents instance;
 
        private NetworkEvents() {}
 
        public static NetworkEvents Instance => instance ?? (instance = new NetworkEvents());

        // EVENTS
        public UnityAction OnConnectToNetwork;
        public UnityAction<string> OnCreateRoom;
        public UnityAction<string> OnJoinRoom;
        public UnityAction<List<RoomInfo>> OnRoomsListUpdated;

        public void Dispose()
        {
            OnConnectToNetwork = null;
            OnCreateRoom = null;
            OnJoinRoom = null;
            OnRoomsListUpdated = null;
        }
    }
}