using UnityEngine.Events;

namespace lab.photon
{
    public class GameEvents
    {
        private static GameEvents instance;
 
        private GameEvents() {}
 
        public static GameEvents Instance => instance ?? (instance = new GameEvents());
        
        // EVENTS
        public UnityAction<GameState, bool> OnGameStateChanged;
        public UnityAction<NetworkRole, bool> OnGoal;
        public UnityAction<ScoreData, bool> OnScoreUpdated;

        public void Dispose()
        {
            OnGameStateChanged = null;
            OnGoal = null;
            OnScoreUpdated = null;
        }
    }
}