using Photon.Pun;
using UnityEngine;

namespace lab.photon
{
    public class BallPUN : MonoBehaviourPun
    {
        [SerializeField] private float force = 8;
        [SerializeField] private float minVerticalVel = 1;
        
        private Rigidbody2D rigidBody;
        private GameSettings gameSettings;
        
        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            
            rigidBody = GetComponent<Rigidbody2D>();
            Respawn();
        }
        
        private void OnCollisionEnter2D(Collision2D col)
        {
            // мяч создан админом
            if (gameSettings.NetworkSettings.NetworkRole != NetworkRole.Admin)
            {
                return;
            }
            
            if (col.gameObject.tag.Equals("BottomWall"))
            {
                GameEvents.Instance.OnGoal?.Invoke(NetworkRole.Admin, true);
                
                Respawn();
            }
            else if (col.gameObject.tag.Equals("UpWall"))
            {
                GameEvents.Instance.OnGoal?.Invoke(NetworkRole.Player, true);
                
                Respawn();
            }
            else
            {
                float addVerticalVel = 0;
                if (Mathf.Abs(rigidBody.velocity.y) < minVerticalVel)
                {
                    addVerticalVel = minVerticalVel * Mathf.Sign(rigidBody.velocity.y);
                }

                if (col.gameObject.tag.Equals("LeftWall"))
                {
                    rigidBody.velocity = new Vector3(force, rigidBody.velocity.y + addVerticalVel, 0);
                }
                else if (col.gameObject.tag.Equals("RightWall"))
                {
                    rigidBody.velocity = new Vector3(-force, rigidBody.velocity.y + addVerticalVel, 0);
                }
            }
        }
        
        private void Respawn()
        {
            transform.position = new Vector2(0f, 0f);
            rigidBody.velocity = new Vector2(Mathf.Sign(Random.Range(-1f, 1f)) * force,Mathf.Sign(Random.Range(-1f, 1f)) * force);
        }
        
        // photonView.RPC("RPC_Respawn", RpcTarget.Others);
    }
}
