using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

namespace lab.photon
{
    public class UIStatePanel : MonoBehaviour
    {
        public UnityAction OnEnter;
        public UnityAction OnExit;
        
        public Button[] NextButtons;
        public Button[] PrevButtons;

        private void OnEnable()
        {
            OnEnter?.Invoke();
        }

        private void OnDisable()
        {
            OnExit?.Invoke();
        }
    }
}