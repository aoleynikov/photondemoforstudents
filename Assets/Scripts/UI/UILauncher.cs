using System.Collections;
using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;
using UnityEngine.UI;

namespace lab.photon
{
    public class UILauncher : MonoBehaviour
    {
        [SerializeField] private InputField roomName;
        [SerializeField] private Button createRoomBtn;
        
        private void Awake()
        {
            createRoomBtn?.onClick.AddListener(delegate { NetworkEvents.Instance.OnCreateRoom?.Invoke(roomName.text); });

            var roleState = GetComponentInChildren<RoleState>(true);
            if (roleState)
            {
                roleState.OnEnter += OnEnter;
            }
        }

        private void OnEnter()
        {
            NetworkEvents.Instance.OnConnectToNetwork?.Invoke();
        }
    }
}