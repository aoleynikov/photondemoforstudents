using System.Collections.Generic;
using Photon.Realtime;
using UnityEngine;

namespace lab.photon
{
    public class UIRoomsViewer : MonoBehaviour
    {
        private JoinRoomState joinRoomState;
        private Dictionary<string, RoomInfo> cachedRoomList;
        private Dictionary<string, GameObject> roomListEntries;

        private void Awake()
        {
            joinRoomState = GetComponentInChildren<JoinRoomState>(true);
            if (!joinRoomState)
            {
                return;
            }
            
            cachedRoomList = new Dictionary<string, RoomInfo>();
            roomListEntries = new Dictionary<string, GameObject>();
            
            NetworkEvents.Instance.OnRoomsListUpdated += OnRoomsListUpdated;
        }
        
        private void OnRoomsListUpdated(List<RoomInfo> roomList)
        {
            Debug.Log("Room list received " + roomList.Count);
            
            ClearRoomListView();

            UpdateCachedRoomList(roomList);
            UpdateRoomListView();
        }
        
        private void ClearRoomListView()
        {
            foreach (GameObject entry in roomListEntries.Values)
            {
                Destroy(entry.gameObject);
            }

            roomListEntries.Clear();
        }
        
        private void UpdateCachedRoomList(List<RoomInfo> roomList)
        {
            foreach (RoomInfo info in roomList)
            {
                // Remove room from cached room list if it got closed, became invisible or was marked as removed
                if (!info.IsOpen || !info.IsVisible || info.RemovedFromList)
                {
                    if (cachedRoomList.ContainsKey(info.Name))
                    {
                        cachedRoomList.Remove(info.Name);
                    }

                    continue;
                }

                // Update cached room info
                if (cachedRoomList.ContainsKey(info.Name))
                {
                    cachedRoomList[info.Name] = info;
                }
                // Add new room info to cache
                else
                {
                    cachedRoomList.Add(info.Name, info);
                }
            }
        }

        private void UpdateRoomListView()
        {
            foreach (RoomInfo info in cachedRoomList.Values)
            {
                GameObject entry = Instantiate(joinRoomState.RoomListEntryPrefab.gameObject, joinRoomState.ContentHolder.transform, false);
                entry.transform.localScale = Vector3.one;
                
                entry.GetComponent<RoomListEntry>().RoomName.text = info.Name;
                entry.GetComponent<RoomListEntry>().ConnectButton.onClick.AddListener(delegate { OnConnectRoomPressed(info.Name); });

                roomListEntries.Add(info.Name, entry);
            }
        }

        private void OnConnectRoomPressed(string roomName)
        {
            NetworkEvents.Instance.OnJoinRoom?.Invoke(roomName);
        }
    }
}