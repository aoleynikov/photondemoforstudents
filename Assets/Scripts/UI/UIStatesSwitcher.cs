using System.Collections.Generic;
using UnityEngine;

namespace lab.photon
{
    [RequireComponent(typeof(Animator))]
    public class UIStatesSwitcher : MonoBehaviour
    {
        private Animator animator;
     
        private static readonly int nextParam = Animator.StringToHash("Next");
        private static readonly int backParam = Animator.StringToHash("Back");
        
        private void Awake()
        {
            animator = GetComponent<Animator>();
            
            UIStatePanel[] uiStatePanels = GetComponentsInChildren<UIStatePanel>(true);

            foreach (var uiState in uiStatePanels)
            {
                if (uiState.NextButtons.Length > 0)
                {
                    foreach (var nextButton in uiState.NextButtons)
                    {
                        nextButton.onClick.AddListener(OnNextButtonClicked);
                    }
                }
                
                if (uiState.PrevButtons.Length > 0)
                {
                    foreach (var prevButton in uiState.PrevButtons)
                    {
                        prevButton.onClick.AddListener(OnPrevButtonClicked);
                    }
                }
            }
        }
        
        private void OnNextButtonClicked()
        {
            SwitchStates(nextParam);
        }

        private void OnPrevButtonClicked()
        {
            SwitchStates(backParam);
        }

        private void SwitchStates(int param)
        {
            animator.SetTrigger(param);
        }
        
    }
}