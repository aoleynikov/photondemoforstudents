using UnityEngine;
using UnityEngine.UI;

namespace lab.photon
{
    [RequireComponent(typeof(Button))]
    public class SetNetworkRoleByButtonAndSave : MonoBehaviour
    {
        [SerializeField] private NetworkRole networkRole;
        private GameSettings gameSettings;
        
        private Button button;

        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            
            button = GetComponent<Button>();
            
            button.onClick.AddListener(SetRole);
        }

        private void SetRole()
        {
            gameSettings.NetworkSettings.NetworkRole = networkRole;
        }
    }
}