using UnityEngine;
using UnityEngine.UI;

namespace lab.photon
{
    public class ButtonAnimatorBoolSetter: MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private string paramName;
        [SerializeField] private bool value;

        private Button button;

        private void Awake()
        {
            button = GetComponent<Button>();
            button.onClick.AddListener(SetValue);
        }

        private void OnDestroy()
        {
            button.onClick.RemoveAllListeners();
        }

        private void SetValue()
        {
            if (!animator)
                return;

            animator.SetBool(paramName, value);
        }
    }
}