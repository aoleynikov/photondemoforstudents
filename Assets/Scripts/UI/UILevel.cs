using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace lab.photon
{
    public class UILevel : MonoBehaviour
    {
        [SerializeField] private Text score;
        [SerializeField] private Button restartButton;

        private GameSettings gameSettings;
        private Animator statesAnimator;
        private GameState curState = GameState.Wait;
        
        private static readonly int waitParam = Animator.StringToHash("Wait");
        private static readonly int gameParam = Animator.StringToHash("Game");
        private static readonly int finishParam = Animator.StringToHash("Finish");
        
        private Dictionary<GameState, int> statesToAnimations = new Dictionary<GameState, int>();

        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            
            statesAnimator = GetComponentInChildren<Animator>();
            
            statesToAnimations.Add(GameState.Wait, waitParam);
            statesToAnimations.Add(GameState.Game, gameParam);
            statesToAnimations.Add(GameState.Finish, finishParam);
            
            GameEvents.Instance.OnScoreUpdated += OnScoreUpdated;
            GameEvents.Instance.OnGameStateChanged += OnGameStateChanged;

            if (gameSettings.NetworkSettings.NetworkRole != NetworkRole.Admin)
            {
                restartButton.gameObject.SetActive(false);
            }
            else
            {
                restartButton.onClick.AddListener(delegate { GameEvents.Instance.OnGameStateChanged?.Invoke(GameState.Game, true); });
            }
        }

        private void OnGameStateChanged(GameState state, bool owner)
        {
            if (curState == state || !statesAnimator || !statesToAnimations.ContainsKey(state))
            {
                return;
            }
            
            statesAnimator.SetTrigger(statesToAnimations[state]);
        }

        private void OnScoreUpdated(ScoreData scoreData, bool owner)
        {
            score.text = scoreData.AdminScore + ":" + scoreData.PlayerScore;
        }
    }
}