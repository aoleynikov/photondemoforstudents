using UnityEngine;
using UnityEngine.UI;

namespace lab.photon
{
    public class RoomListEntry : MonoBehaviour
    {
        public Text RoomName;
        public Button ConnectButton;
    }
}