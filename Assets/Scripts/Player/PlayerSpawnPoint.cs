using UnityEngine;

namespace lab.photon
{
    public enum SpawnPoint
    {
        MasterClient,
        Client
    }
    
    public class PlayerSpawnPoint : MonoBehaviour
    {
        public SpawnPoint SpawnPointType;
    }
}