using System.Collections.Generic;
using Photon.Pun;
using UnityEngine;

namespace lab.photon
{
    public class PlayerCreator : MonoBehaviour
    {
        [SerializeField] private List<string> playerPrefabNames;
        [SerializeField] private string spectatorPrefabName;
        
        private GameObject player;
        private GameSettings gameSettings;
        
        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            
            if (gameSettings.NetworkSettings.NetworkRole == NetworkRole.Admin || gameSettings.NetworkSettings.NetworkRole == NetworkRole.Player)
            {
                SpawnPlayer();
            }
            else
            {
                SpawnSpectator();
            }
        }

        private void SpawnPlayer()
        {
            Vector3 spawnPos = Vector3.zero;
            
            foreach (var spawnPoint in FindObjectsOfType<PlayerSpawnPoint>())
            {
                if (spawnPoint.SpawnPointType == SpawnPoint.MasterClient && gameSettings.NetworkSettings.NetworkRole == NetworkRole.Admin)
                {
                    spawnPos = spawnPoint.transform.position;
                    break;
                }
                else if (spawnPoint.SpawnPointType == SpawnPoint.Client && gameSettings.NetworkSettings.NetworkRole == NetworkRole.Player)
                {
                    spawnPos = spawnPoint.transform.position;
                    break;
                }
            }

            SpawnPlayerAtPosition(spawnPos);
        }

        private void SpawnPlayerAtPosition(Vector3 pos)
        {
            player = PhotonNetwork.Instantiate("Prefabs/Players Variants/" + playerPrefabNames[Random.Range(0, playerPrefabNames.Count)], pos, Quaternion.identity, 0);
        }

        private void SpawnSpectator()
        {
            player = PhotonNetwork.Instantiate("Prefabs/" + spectatorPrefabName, Vector3.zero, Quaternion.identity, 0);
        }
        
    }
}