using Photon.Pun;
using UnityEngine;

namespace lab.photon
{
    public class PlayerPUN : MonoBehaviourPun
    {       
        private Camera cam;
        private Vector3 offset;
        
        private readonly float minScreenX = -6f;
        private readonly float maxScreenX = 6f;
        
        private void Awake()
        {
            cam = Camera.main;
        }
        
        private void OnMouseDown()
        {
            if (!photonView.IsMine)
            {
                return;
            }

            offset = gameObject.transform.position - cam.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, 10.0f));
        }
        
        private void OnMouseDrag()
        {
            if (!photonView.IsMine)
            {
                return;
            }

            Vector3 newPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 0);
            Vector2 newPos = cam.ScreenToWorldPoint(newPosition) + offset;
            newPos.x = Mathf.Clamp(newPos.x, minScreenX, maxScreenX);

            transform.position = new Vector2(newPos.x, transform.position.y);
        }
    }
}