using System.Collections.Generic;
using System.Xml.Serialization;
using Photon.Pun;
using UnityEngine;
using UnityEngine.Events;

namespace lab.photon
{
    public class GameSessionRunner : MonoBehaviour
    {
        [SerializeField] private string ballPrefabName;

        private GameSettings gameSettings;
        private GameState curState = GameState.Wait;
        private GameObject ball;

        private int adminScore = 0;
        private int playerScore = 0;
        
        private Dictionary<GameState, UnityAction> actionsByStates = new Dictionary<GameState, UnityAction>();
        
        private void Awake()
        {
            gameSettings = FindObjectOfType<GameSettings>();
            GameEvents.Instance.OnGameStateChanged += OnGameStateChanged;
            GameEvents.Instance.OnGoal += OnGoal;

            if (gameSettings.NetworkSettings.NetworkRole == NetworkRole.Admin)
            {
                actionsByStates.Add(GameState.Wait, DestroyBall);
                actionsByStates.Add(GameState.Game, delegate
                {
                    ResetScore();
                    SpawnBall();
                });
                actionsByStates.Add(GameState.Finish, DestroyBall);
            }
        }

        private void OnDestroy()
        {
            GameEvents.Instance.Dispose();
        }

        private void OnGameStateChanged(GameState state, bool owner)
        {
            curState = state;

            if (actionsByStates.ContainsKey(state))
            {
                actionsByStates[state].Invoke();
            }
        }

        private void ResetScore()
        {
            playerScore = 0;
            adminScore = 0;
            var scoreData = new ScoreData {AdminScore = adminScore, PlayerScore = playerScore};
            GameEvents.Instance.OnScoreUpdated?.Invoke(scoreData, true);
        }
        
        private void SpawnBall()
        {
            ball = PhotonNetwork.Instantiate("Prefabs/Ball/" + ballPrefabName, Vector3.zero, Quaternion.identity, 0);
        }

        private void DestroyBall()
        {
            if (ball)
            {
                PhotonNetwork.Destroy(ball);
            }
        }
        
        private void OnGoal(NetworkRole networkRole, bool owner)
        {
            switch (networkRole)
            {
                case NetworkRole.Admin:
                    playerScore++;
                    break;
                
                case NetworkRole.Player:
                    adminScore++;
                    break;
            }

            var scoreData = new ScoreData {AdminScore = adminScore, PlayerScore = playerScore};

            GameEvents.Instance.OnScoreUpdated?.Invoke(scoreData, owner);

            if (playerScore == gameSettings.LevelSettings.MaxScore ||
                adminScore == gameSettings.LevelSettings.MaxScore)
            {
                GameEvents.Instance.OnGameStateChanged?.Invoke(GameState.Finish, true);
            }
        }
    }
}